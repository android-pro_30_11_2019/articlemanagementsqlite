package com.example.articlesqlite.db;

public final class DBConstant {
    //Database
    public static final String DB_NAME = "article_management.db";
    public static final int DB_VERSION = 1;

    //TODO: Create Table "tbarticle";
    public static final String CREATE_TBARTICLE_SQL = "CREATE TABLE "+ DB.TbArticleENTRY.TABLE_NAME + " ( "
            + DB.TbArticleENTRY._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            DB.TbArticleENTRY.COLUMN_TITLE + " TEXT NOT NULL, "+
            DB.TbArticleENTRY.COLUMN_DESCRIPTION + " TEXT NOT NULL)";

    //TODO: Create Table "tbuser";
    public static final String CREATE_TBUSER_SQL = "CREATE TABLE "+ DB.TbUserENTRY.TABLE_NAME + " ( " +
            DB.TbArticleENTRY._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            DB.TbUserENTRY.COLUMN_FULLNAME + " TEXT NOT NULL, "+
            DB.TbUserENTRY.COLUMN_GENDER + " TEXT NOT NULL, " +
            DB.TbUserENTRY.COLUMN_USERNAME + " TEXT NOT NULL," +
            DB.TbUserENTRY.COLUMN_PASSWORD + " TEXT NOT NULL)";
}
