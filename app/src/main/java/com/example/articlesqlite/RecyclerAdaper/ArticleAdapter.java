package com.example.articlesqlite.recycleradaper;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.articlesqlite.R;
import com.example.articlesqlite.model.Article;

import java.util.ArrayList;
import java.util.List;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleViewHolder> {
    private List<Article> articles;
    private ArticleListener listener;

    public ArticleAdapter() {
        this.articles = new ArrayList<>();
    }

    public void setListener(ArticleListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ArticleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_article,parent,false);
        return new ArticleViewHolder(view,listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleViewHolder holder, int position) {
        holder.onBind(articles.get(position));
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }
    public  void AddMoreItem(List<Article> articles){
        this.articles.addAll(articles);
        notifyDataSetChanged();
    }

    public void clear() {
        this.articles.clear();
    }
}
