package com.example.articlesqlite.recycleradaper;

public interface ArticleListener {
    void onArticleDeleteClick(int position);
    void onArticleClick(int position);
}
