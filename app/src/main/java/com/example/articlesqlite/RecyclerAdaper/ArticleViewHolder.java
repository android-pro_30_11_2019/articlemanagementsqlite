package com.example.articlesqlite.recycleradaper;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.articlesqlite.R;
import com.example.articlesqlite.model.Article;

public class ArticleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private final ArticleListener listener;
    private TextView tvTitle,tvdescription;
    private ImageButton btnDel;

    public ArticleViewHolder(@NonNull View item_article,final ArticleListener listener) {
        super(item_article);
        tvTitle = item_article.findViewById(R.id.tvTitle);
        tvdescription = item_article.findViewById(R.id.tvDescription);
        btnDel = item_article.findViewById(R.id.btnDel);

        this.listener = listener;
        btnDel.setOnClickListener(this);
        item_article.setOnClickListener(this);
    }

    public void onBind(Article article) {
        tvTitle.setText(article.getTitle());
        tvdescription.setText(article.getDescription());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnDel:
                listener.onArticleDeleteClick(getAdapterPosition());
                break;
                default:
                    listener.onArticleClick(getAdapterPosition());
        }
    }
}
